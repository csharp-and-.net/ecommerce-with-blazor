﻿using Ecommerce.Abstractions;
using Ecommerce.Persistence.Contracts;

namespace Ecommerce.Application
{
    public class Application<T> : ICrud<T> where T : IEntity
    {
        private readonly IBaseRepository<T> _repository;

        public Application(IBaseRepository<T> repository)
        {
            _repository = repository;
        }

        public void Delete(int id)
        {
            _repository.Delete(id);
        }

        public IEnumerable<T> GetAll()
        {
            return _repository.GetAll();
        }

        public Task<T> GetById(int id)
        {
            return _repository.GetById(id);
        }

        public T Save(T entity)
        {
            return _repository.Save(entity);
        }
                
    }
}
