﻿using Ecommerce.Domain;
using Ecommerce.Shared.Responses;

namespace Ecommerce.Application.Contracts
{
    public interface IApplication<T>
    {
        GenericResponse<IEnumerable<Product>> GetAll();
        Task<GenericResponse<Product>> GetByIdAsync(int id);
    }
}
