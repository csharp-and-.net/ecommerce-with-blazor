﻿using Ecommerce.Domain;

namespace Ecommerce.Application.Contracts
{
    public interface IProductApplication : IApplication<Product>
    {

    }
}
