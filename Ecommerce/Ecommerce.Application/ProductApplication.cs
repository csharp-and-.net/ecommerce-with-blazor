﻿using Ecommerce.Application.Contracts;
using Ecommerce.Domain;
using Ecommerce.Persistence;
using Ecommerce.Shared.Responses;

namespace Ecommerce.Application
{
    public class ProductApplication : IProductApplication
    {
        private readonly IUnitOfWork _unitOfWork;

        public ProductApplication(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public GenericResponse<IEnumerable<Product>> GetAll()
        {
            var products = _unitOfWork.ProductRepository?.GetAll();
            var response = new GenericResponse<IEnumerable<Product>>()
            {
                Data = products
            };
            return response;
        }

        public async Task<GenericResponse<Product>> GetByIdAsync(int id)
        {
            var response = new GenericResponse<Product>();
            var product = await _unitOfWork.ProductRepository?.GetById(id);
            if (product == null)
            {
                response.Success = false;
                response.Message = "Sorry the product doesn't exist.";
            }
            else
            {
                response.Data = product;
            }
            return response;
        }
    }
}
