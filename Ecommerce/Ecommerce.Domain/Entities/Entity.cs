﻿using Ecommerce.Abstractions;

namespace Ecommerce.Domain.Entities
{
    public abstract class Entity : IEntity
    {
        public int Id { get; set; }
    }
}
