﻿using Ecommerce.Abstractions;
using Ecommerce.Domain.Entities;

namespace Ecommerce.Domain
{
    public class Product : IEntity
    {
        public int Id { get; set; }
        public string Title { get; set; } = null!;
        public string Description { get; set; } = null!;
        public string ImageUrl { get; set; } = null!;
        public decimal Price { get; set; }

        public Category? Category { get; set; }
        public int CategoryId { get; set; }
    }
}
