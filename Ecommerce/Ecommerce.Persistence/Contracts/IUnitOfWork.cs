﻿using Ecommerce.Domain;
using Ecommerce.Persistence.Contracts;

namespace Ecommerce.Persistence
{
    public interface IUnitOfWork : IDisposable
    {
        IProductRepository? ProductRepository { get; }
        void SaveChanges();
        Task SaveChangesAsync();
    }
}
