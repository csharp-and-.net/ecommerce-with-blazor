﻿using Ecommerce.Domain;

namespace Ecommerce.Persistence.Contracts
{
    public interface IProductRepository : IBaseRepository<Product>
    {
    }
}
