﻿using Ecommerce.Abstractions;

namespace Ecommerce.Persistence.Contracts
{
    public interface IBaseRepository<T> : ICrud<T>
    { 

    }
}
