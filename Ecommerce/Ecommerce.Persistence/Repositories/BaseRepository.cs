﻿using Ecommerce.Abstractions;
using Ecommerce.Persistence.Contracts;

namespace Ecommerce.Persistence
{
    public class BaseRepository<T> : IBaseRepository<T> where T : IEntity
    {
        private readonly IDbContext<T> _ctx;
        public BaseRepository(IDbContext<T> dbContext)
        {
            _ctx = dbContext;

        }

        public void Delete(int id)
        {
            _ctx.Delete(id);
        }

        public IEnumerable<T> GetAll()
        {
            return _ctx.GetAll();
        }

        public async Task<T> GetById(int id)
        {
            return await _ctx.GetById(id);
        }

        public T Save(T entity)
        {
            return _ctx.Save(entity);
        }
    }
}
