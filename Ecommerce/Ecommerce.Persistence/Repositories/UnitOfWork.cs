﻿using Ecommerce.Abstractions;
using Ecommerce.Domain;
using Ecommerce.Persistence.Contracts;

namespace Ecommerce.Persistence.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDbContext<Product> _ctx;
        private readonly IProductRepository? _productRepository;

        public UnitOfWork(IDbContext<Product> ctx)
        {
            _ctx = ctx;
        }
              
        public IProductRepository ProductRepository => _productRepository ?? new ProductRepository(_ctx);

        public void Dispose()
        {
                _ctx.Dispose();
        }

        public void SaveChanges()
        {
            _ctx.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _ctx.SaveChangesAsync();
        }
    }
}
