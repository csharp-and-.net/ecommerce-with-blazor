﻿using Ecommerce.Abstractions;
using Ecommerce.Domain;
using Ecommerce.Persistence.Contracts;

namespace Ecommerce.Persistence.Repositories
{
    public class ProductRepository : BaseRepository<Product>, IProductRepository
    {
        public ProductRepository(IDbContext<Product> dbContext) : base(dbContext)
        {
        }
    }
}
