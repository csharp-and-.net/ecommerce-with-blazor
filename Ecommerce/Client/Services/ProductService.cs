﻿namespace Ecommerce.Client.Services
{
    public class ProductService : IProductService
    {
        private readonly HttpClient _http;
        public ProductService(HttpClient http)
        {
            _http = http;
        }
        public List<ProductModel> Products { get; set; } = new();

        public async Task<GenericResponse<ProductModel>> GetProductAsync(int productId)
        {
            var result = await _http.GetFromJsonAsync<GenericResponse<ProductModel>>($"api/Product/{productId}");
            return result;
        }

        public async Task GetProductsAsync()
        {
            var result = await _http.GetFromJsonAsync<GenericResponse<List<ProductModel>>>("api/Product");
            if (result != null && result.Data != null) Products = result.Data;

        }
    }
}
