﻿namespace Ecommerce.Client.Services
{
    public interface IProductService
    {
        List<ProductModel> Products { get; set; }
        Task GetProductsAsync();
        Task<GenericResponse<ProductModel>> GetProductAsync(int productId);
    }
}
