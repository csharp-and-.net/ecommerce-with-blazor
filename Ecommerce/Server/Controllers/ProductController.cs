﻿using Ecommerce.Application.Contracts;
using Ecommerce.Domain;
using Ecommerce.Shared.DTOs;
using Ecommerce.Shared.Models;
using Microsoft.AspNetCore.Mvc;

namespace Ecommerce.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductApplication _productApplication;

        public ProductController(IProductApplication product)
        {
            _productApplication = product;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var response = _productApplication.GetAll();
            return Ok(response);
        }

        [HttpGet("{productId}")]        
        public async Task<IActionResult> GetProductAsync(int productId)
        {
            var response = await _productApplication.GetByIdAsync(productId);
            return Ok(response);
        }

        //[HttpPost]
        //public IActionResult Save(ProductDTO dto)
        //{
        //    var p = new ProductModel()
        //    {
        //        Title= dto.Title,
        //        Description=dto.Description,
        //        ImageUrl=dto.ImageUrl,
        //        Price=dto.Price
        //    };
        //    return Ok(_product.Save(p));
        //}
    }
}

