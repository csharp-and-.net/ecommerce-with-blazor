﻿namespace Ecommerce.Abstractions
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
