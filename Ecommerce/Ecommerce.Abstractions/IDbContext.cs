﻿namespace Ecommerce.Abstractions
{
    public interface IDbContext<T> : ICrud<T>
    {
        void Dispose();
        void SaveChanges();
        Task SaveChangesAsync();

    }
}
