﻿namespace Ecommerce.Abstractions
{
    public interface ICrud<T>
    {
        T Save(T entity);
        IEnumerable<T> GetAll();
        Task<T> GetById(int id);
        void Delete(int id);

    }
}