﻿using Ecommerce.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace Ecommerce.DataAccess
{
    public class DbContext<T> : IDbContext<T> where T : class, IEntity
    {
        private readonly DbSet<T> _items;
        private readonly ApiDbContext _ctx;

        public DbContext(ApiDbContext ctx)
        {
            _ctx = ctx;
            _items = ctx.Set<T>();
        }

        public void Delete(int id)
        {

        }

        public void Dispose()
        {
            if (_ctx != null)
            {
                _ctx.Dispose();
            }
        }

        public IEnumerable<T> GetAll()
        {
            return _items.AsEnumerable();
        }

        public async Task<T> GetById(int id)
        {
            return await _items.FindAsync(id);
        }

        public T Save(T entity)
        {
            _items.Add(entity);
            _ctx.SaveChanges();
            return entity;
        }

        public void SaveChanges()
        {
            _ctx.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _ctx.SaveChangesAsync();
        }
    }
}
